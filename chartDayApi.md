###### dummy data available from `2019-03-01` to `2019-03-31`

### url: ``104.156.59.186:8894/dayDataWeb``
### post data (for testing with dummy data): 
```json
    {
		"token": "WIZZ-test",
		"startTime": 1523099299,
		"endTime": 1554203359
		
	}
```

### post data: 
```json
    {
		"token": "WIZZ",
		"startTime": 1523099299,
		"endTime": 1554203359
		
	}
```

#### some timestamps for testing

2018-03-01: ``1551398400``  

2018-03-02: ``1551484800``  

2018-03-03: ``1551571200``  

2018-03-10: ``1552176000``  

2018-03-27: ``1553644800``  

2018-03-28: ``1553731200``  

2018-03-29: ``1553817600`` 