# Dex Private Endpoints

Dex private  endpoints can be used to perform user based private actions.  

## /signUp

user will sign up using username and password that will be stored on `jwt_users` table.  

* `success` in response can be used to check if the api call was successful or not.  

#### url: `wizz.network:4548/signUp`  

#### post data: 
```json
{
	"username": "test",
	"password": "test"
}
```
#### response: 

```json
{
  "success": true,
  "result": "Kindly Login to continue."
}

```  


## /logIn

Upon providing correct login credentials an access token will be provided in response that will be used for user authentication in rest of private endpoints.  

* `success` in response can be used to check if the api call was successful or not.  

#### url: `wizz.network:4548/logIn`  

#### post data (body): 
```json
{
	"username": "test",
	"password": "test"
}
```

#### response: 

```json
{
  "success": true,
  "result": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoidGVzdHdpenpuZXQxIn0sImlhdCI6MTU1NjcwOTcwNSwiZXhwIjoxNTU2NzEwNjA1fQ.O7s8a4GuIwlXvM3Nf5I6ZUw-4TKSxzc4P_xiByB9yyg"
}

```


## /getMyBids

This endpoint responses all the `Pending` & `Invalid` bids of user that is calling this end point. The data is collected from `SELL_USERS` and `BUY_USERS` tables.  


* The acces token from the response of `/logIn` will be passed as header which will be used for user authertication. 
* `success` in response can be used to check if the api call was successful or not.  

#### url: `wizz.network:4548/getMyBids`  

#### post data (header): 
```
Content-Type: application/json
x-access-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoidGVzdHdpenpuZXQxIn0sImlhdCI6MTU1NjcwNzE2NiwiZXhwIjoxNTU2NzA4MDY2fQ.mpJq6Hj68teO-45XOrsb1BYobTA3TQsylFvZzA6QfLk

```
#### post data (body): 
```
```

#### response: 
```json
{
  "success": true,
  "result": [
    {
      "id": 1440,
      "price": 0.000111,
      "token_name": "WIZZ",
      "amount": 0.0901,
      "created_at": "2019-04-26T10:26:02.000Z",
      "type": "BUY",
      "status": "Invalid"
    }
  ]
}
```

## /getMyTradeHistory

This endpoint responses all the `Revoke` & `Traded` bids of user that is calling this end point. The data is collected from `SELL_USERS` and `BUY_USERS` tables.  

* `dealt` is calculated on runtime which is equal to the amount that is transfered (`transfer`) out of total amount.
* `amount` is sum of bid `amount` and bid amount that is transered `trasfer`.
* `dealt_percentage` is calcuated as the % of amount that is dealt of total amount.
* `total` reprisents the `value` of bid.
* The acces token from the response of `/logIn` will be passed as header which will be used for user authertication. 
* `success` in response can be used to check if the api call was successful or not.  

#### url: `wizz.network:4548/getMyTradeHistory`  

#### post data (header): 
```
Content-Type: application/json
x-access-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoidGVzdHdpenpuZXQxIn0sImlhdCI6MTU1NjcwNzE2NiwiZXhwIjoxNTU2NzA4MDY2fQ.mpJq6Hj68teO-45XOrsb1BYobTA3TQsylFvZzA6QfLk

```
#### post data (body): 
```
```

#### response: 
```json
{
  "success": true,
  "result": {
    "traded": [
      {
        "id": 1234,
        "price": 0.0001111,
        "token_name": "WIZZ",
        "amount": 100,
        "created_at": "2019-04-30T11:10:45.000Z",
        "type": "SELL",
        "status": "traded",
        "dealt": 100,
        "total": 0,
        "dealt_percentage": "100.0000"
      }
    ], 
    "revoked": [
      {
        "id": 1447,
        "price": 0.0001111,
        "token_name": "WIZZ",
        "amount": 100,
        "created_at": "2019-04-30T15:19:11.000Z",
        "type": "BUY",
        "status": "revoke",
        "dealt": 0,
        "total": 0.0111,
        "dealt_percentage": "0.0000"
      }
    ]
   }
}
```



## /buyBid

User can place Buy bid for a specific token using this call, EOS token from user's account is used. the transaction is performed on eos blockchains and returns a transaction id in response.  

* `price` is option. if provided then it will be considered as price for bid. otherwise the lateset price token being bought will be fetched from `trade` table. 
* `pk` is required as its a blockchain based transaction.
* `value` is calcuated as  `price`* `amount`.
* `value` should be greater than `0.0001`.
* The acces token from the response of `/logIn` will be passed as header which will be used for user authertication. 
* `success` in response can be used to check if the api call was successful or not.  

#### url: `wizz.network:4548/buyBid`  

#### post data (header): 
```
Content-Type: application/json
x-access-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoidGVzdHdpenpuZXQxIn0sImlhdCI6MTU1NjcwNzE2NiwiZXhwIjoxNTU2NzA4MDY2fQ.mpJq6Hj68teO-45XOrsb1BYobTA3TQsylFvZzA6QfLk

```
#### post data (body): 
```json
{
	"pk": "njshfhvajnja",
	"symbol": "WIZZ",
	"amount": 20,
	"price": 10
}
```

#### response: 
```json
{
  "success": true,
  "result": {
    "trx_id": "d50b345e2ecc76a4251023f74e8cb3d63e56c04d0415feb81329e9f78e6a958f"
  }
}
```


## /sellBid

User can place Sell bid to sell specific token using this call, specifed token from user's account is used. the transaction is performed on eos blockchains and returns a transaction id in response.  

* `price` is option. if provided then it will be considered as price for bid. otherwise the lateset price token being bought will be fetched from `trade` table. 
* `pk` is required as its a blockchain based transaction.
* `value` is calcuated as  `price`* `amount`.
* `value` should be greater than `0.0001`.
* The acces token from the response of `/logIn` will be passed as header which will be used for user authertication. 
* `success` in response can be used to check if the api call was successful or not.  

#### url: `wizz.network:4548/sellBid`  

#### post data (header): 
```
Content-Type: application/json
x-access-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoidGVzdHdpenpuZXQxIn0sImlhdCI6MTU1NjcwNzE2NiwiZXhwIjoxNTU2NzA4MDY2fQ.mpJq6Hj68teO-45XOrsb1BYobTA3TQsylFvZzA6QfLk

```
#### post data (body): 
```json
{
	"pk": "fsjhfjnjnan",
	"token": "WIZZ",
	"amount": 20,
	"price": 10
}
```

#### response: 
```json
{
  "success": true,
  "result": {
    "trx_id": "5b87af1aaafc5a079ec7e6a690342aabca2c1c7d0432ce13815f32f8d0ab4e41"
  }
}
```


## /revokeBid

User can calcel/revoke his `Sell` or `Buy` bids by providing the id of bid. on success the bid is marked as revoke in `SELL_USERS` or `BUY_USERS` table and the bid's amount and value is deducted from `BUYS` or `SELLS` table using price of bid as identifier. and then the amount is returned to user using blockchain transaction, returning a transaction id in response.  


* if the provided bid is of `sell` type then EOS will be returned to user. else in case of `buy` type the token used in bid will be returned.
* The acces token from the response of `/logIn` will be passed as header which will be used for user authertication. 
* `success` in response can be used to check if the api call was successful or not.  

#### url: `wizz.network:4548/revokeBid`  

#### post data (header): 
```
Content-Type: application/json
x-access-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoidGVzdHdpenpuZXQxIn0sImlhdCI6MTU1NjcwNzE2NiwiZXhwIjoxNTU2NzA4MDY2fQ.mpJq6Hj68teO-45XOrsb1BYobTA3TQsylFvZzA6QfLk

```
#### post data (body): 
```json
{
	"bid_id": 1228,
	"type": "BUY"
}
```

#### response: 
```json
{
  "success": true,
  "result": {
    "trx_id": "8282a7f353d28bd335df42612c26494cab01562fbf309a9643e68c58bee548c3"
  }
}
```